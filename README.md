# Cheap CNC #

A fully open-source desktop CNC machine.

## State ##
alpha

## Status ##
- All X, Y axis parts built and integrated with steppers in their first version
- Driver board (PCB) and parts available, assembly is ongoing 
- Z axis parts ordered