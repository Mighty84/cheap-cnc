#define F_CPU 16000000 /* Hz */

#define AVR_CLASSIC 0

#define UART_ECHO_MODE 1 /* echo incoming characters */
#define UART_BAUD_RATE 9600  /* 9600 Baud */

#define true 1
#define false 0
