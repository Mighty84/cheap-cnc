#include "avr.h"

void usleep(int us)
{
	float f_us = us * (F_CPU/(2*1000000));

	/* crop! */
	us = (int) f_us;
	
	/* should occupy 2 cycles */
	while(us) --us;
}
	
void sleep(int s)
{
	float f_s = s * (F_CPU/2);

	/* crop! */
	s = (int) f_s;

	usleep(s);
}

