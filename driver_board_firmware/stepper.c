#include "avr.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "sleep.h"

// Pinning on L298 Board is OUT1 = Yellow, OUT2 = Blue, OUT3 = Green, OUT4 = Red

void stepper_init()
{
    DDRA = 0xFF;
    //TCCR0 = 1;             // Timer läuft mit vollem Systemtakt
    //TIMSK |= (1<<TOIE0);   // Interrupt freischalten
    //sei();
}

// Full steps
char fsteps[] = {
    0xA0, // 1010 0000
    0x60, // 0110 0000
    0x50, // 0101 0000
    0x90  // 1001 0000
};

// Half-step
char hsteps[] = {
    0xA0, // 1010 0000
    0x20, // 0010 0000
    0x60, // 0110 0000
    0x40, // 0100 0000
    0x50, // 0101 0000
    0x10, // 0001 0000
    0x90, // 1001 0000
    0x80  // 1000 0000  
};

static int step_index=0;

void stepper_left()
{
    step_index = (step_index+1)%8;
        
    // Half-power
    for(int x = 0; x < 4; x++) {
        PORTA = hsteps[step_index];
        _delay_us(75);
        PORTA = 0x00;
        _delay_us(75);
    }
    
    // Full power
    //PORTA = hstepss[step_index];
    //_delay_us(500);
}

void stepper_right()
{
    step_index = step_index-1;
    if(step_index < 0) 
        step_index = 7;
    
    // Half-power
    for(int x = 0; x < 4; x++) {
        PORTA = hsteps[step_index];
        _delay_us(75);
        PORTA = 0x00;
        _delay_us(75);
    }
    
    // Full power
    //PORTA = hstepss[step_index];
    //_delay_us(500);
}

#if 0
static void ns_tick()
{
    static uint16_t pulse=0;
    
    if(pulse == 0) {
        PORTA = 0xFF;
    }
    
    if(pulse == 10) {
        usleep(val);
        PORTA = 0x0;
    }
    
    // We actually have 200 * 9.6*10^-5 + Val_us >= 20ms
    // But the protocol doesn't really care when or how
    // often a telegram is sent
    if(pulse == 200) {
        // PORTA = (1<<1);
        PORTA = 0xFF;
        pulse = 0;
    }
    
    ++pulse;
}

ISR(TIMER0_OVF_vect) {
    static uint8_t d=0;
    
    if(d == 6) {  // 0.000096s
        ns_tick();
        d = 0;
    }
        
    d++;
}
#endif
