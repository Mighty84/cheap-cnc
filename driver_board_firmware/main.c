#include <avr/io.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "avr.h"
#include "uart.h"
#include "freq.h"
#include "mmc.h"
#include "sleep.h"
#include "pwm.h"
#include "servo.h"
#include "stepper.h"

int main()
{
    uart_init();
    //freq_init();
    
    uart_puts("System ready ...\n");
    //while(1) uart_getc();

#if 1
    stepper_init();
    
    while(1) {
        for(int i = 0; i < 50000; i++)
            stepper_left();
        for(int i = 0; i < 50000; i++)
            stepper_right();
    }
    
#endif

#if 0
    servo_init();
    
    unsigned char cmd[16];
    while(1) {
        uart_puts("> ");
        
        memset(cmd, 0x0, 16);
        uart_gets(cmd, 16);
        int nval = atoi(cmd);
        
        sprintf(cmd, "Setting value: %i\n", nval);
        uart_puts(cmd);
        servo_set(nval);
    }
#endif

#if 0
    DDRA |= (1 << DDA0);

    bool a = 0;
    while(1) {
        if(a) {
            PORTA &= ~(1 << PA0);
            a = 0;
            usleep(20);
        }
        else {
            PORTA |= (1 << PA0);
            a = 1;
            usleep(20);
        }
    }
#endif

#if 0
    pwm_init();
    while(1);
#endif 

#if 0
    unsigned char cmd[16];
    while(1) {
        uart_puts("> ");
        uart_gets(cmd, 16);

        if(strncmp(cmd, "freq", 4) == 0) {
            char val[16];
            uart_puts("Frequency sampled: ");
            sprintf(val, "%i", freq_sample());
            uart_puts(val);
            uart_puts("\n");
        }
    }
#endif

#if 0
    while(mmc_init() !=0)
        uart_puts("No MMC/SD found\n"); 
    
    uart_puts("Card found\n");

    unsigned char buffer[512];
        
    uart_puts("Write a sector\n");
    buffer[0] = 0xDE;
    buffer[1] = 0xAD;
    buffer[2] = 0xBE;
    buffer[3] = 0xEF;
    mmc_write_sector(0, &buffer);

    uart_puts("Read a sector\n");
    buffer[0] = 0x00;
    buffer[1] = 0x00;
    buffer[2] = 0x00;
    buffer[3] = 0x00;
    mmc_read_sector(0, &buffer);
    
    char res[1];
    for(int i = 0; i<8; i++) { 
        itoa(buffer[i], res, 10);
        uart_putc(res[0]);
    }
    uart_putc('\n');

#endif
}
