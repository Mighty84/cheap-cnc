#include <avr/io.h>
#include "uart.h"
#include "avr.h"

void uart_init()
{
	const unsigned int bbr = F_CPU / (UART_BAUD_RATE * 16L) - 1;

	#if AVR_CLASSIC 
		UCR |= (1<<TXEN); /* enable transmit */
		UCR |= (1<<RXEN); /* enable recieve */
	
		UBRR = bbr; /* set baud rate */
	#else	
		UCSRB |= (1<<TXEN); /* enable transmit */
		UCSRB |= (1<<RXEN); /* enable recieve */
 		UCSRC |= (1<<URSEL)|(3<<UCSZ0);	/* asynchonous 8N1 */

		UBRRH = (uint8_t) bbr >> 8; /* set baud rate */
  		UBRRL = (uint8_t) bbr;
	#endif
}

void uart_putc(unsigned char c)
{
	/* wait until register is shifted empty */
	#if AVR_CLASSIC
		while(!(USR & (1 << UDRE)));
	#else	
		while (!(UCSRA & (1<<UDRE)));	
	#endif
	
	/* send character */
	UDR = c;  
}

void uart_puts(char *string)
{
	unsigned int i = 0;
	
	while(string[i]) {
		uart_putc(string[i]);
		++i;
	}
}

unsigned char uart_getc()
{
	/* wait until receptive */
	#if AVR_CLASSIC
		while(!(USR & (1 << RXC)));
	#else
		while (!(UCSRA & (1<<RXC)));
	#endif
		
	/* recieve character */
	unsigned char c = UDR; 	
	
	#if UART_ECHO_MODE
		uart_putc(c);
	#endif
	
	return c;
}

unsigned int uart_gets(unsigned char *string, unsigned int length)
{	
	unsigned int counter = 0;
	unsigned char c;
	
	/* while character not enter 
	 * and while space add to buffer */
	while(((c = uart_getc()) != 0xD) && (counter < length))
		string[counter++] = c;

	return counter;
}
